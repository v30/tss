import re
from simhash import Simhash, SimhashIndex
import csv
import sys
from itertools import islice

csv.field_size_limit(sys.maxsize)

def get_features(s):
    width = 4
    s = s.lower()
    s = re.sub(r'[^\w]+', '', s)
    return [s[i:i + width] for i in range(max(len(s) - width + 1, 1))]

infile = open("scam_unique.csv", 'r')
reader = csv.reader(infile)
data = {}
count = 1
for rows in islice(reader,1,10):
	data[count] = rows[2]
	count += 1

objs = [(str(k), Simhash(get_features(v))) for k, v in data.items()]
index = SimhashIndex(objs, k=3)

print index.bucket_size()

for k,v in data.items():
	s1 = Simhash(get_features(v))
	print index.get_near_dups(s1)
	break

#s1 = Simhash(get_features(u'How are you i am fine. blar blar blar blar blar thank'))
#print index.get_near_dups(s1)

#index.add('4', s1)
#print index.get_near_dups(s1)