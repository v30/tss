from more_itertools import unique_everseen
import csv
import sys
#with open('scam.csv','r') as f, open('scam_unique.csv','w') as out_file:
#out_file.writelines(unique_everseen(f))

if __name__ == '__main__':

	url = set() # set for fast O(1) amortized lookup
	csv.field_size_limit(sys.maxsize)
	infile = open("scam.csv", 'r')
	outfile = open("scam_unique.csv", 'w')
	writer = csv.writer(outfile)
	reader = csv.reader(infile)
	for rows in reader:
	    if rows[0] in url: 
	        continue # skip duplicate
	    url.add(rows[0])
	    writer.writerow([rows[0],rows[1],rows[2]])
	    