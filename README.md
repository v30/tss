Scam Clustering:

custcluster.py
1) Created a manual dump of data from index.db, scam_data table.
2) Data consisted of id, html_code, text_string
3) Created a csv file data structure as an input file.
   run: python custcluster.py <infile> <outfile> <csv_file> <width> <thresh>
   infile => Manual data dump
   outfile => file with clusters of ids
   csv_file => file with id and pure html text 
   width => n-grams
   thresh => near duplicates for similarity parameter for Simhash.

app.py
1) run: python app.py
2) csv_file from above output is used for displayng html text on the UI
3) Phones numbers, domain and IP and fetched directly from the index.db, scam_data table.
