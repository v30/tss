from bs4 import BeautifulSoup
import csv
from itertools import islice
import re
from simhash import Simhash, SimhashIndex
import sys
import copy
import json

reload(sys)
sys.setdefaultencoding('utf-8')

def get_features(s, w):
	width = w
	s = s.lower()
	s = re.sub(r'[^\w]+', '', s)
	return [s[i:i + width] for i in range(max(len(s) - width + 1, 1))]

def isPresent(k, lis):
	flag = False
	for i in lis:
		if int(i) == k:
			flag = True
			break
	return flag

if __name__ == '__main__':
	
	infile = open(sys.argv[1], 'r')
	outfile = open(sys.argv[2], 'w')
	csv_file = open(sys.argv[3], 'w')
	width = int(sys.argv[4])
	thresh = int(sys.argv[5])
	reader = csv.reader(infile)
	writer = csv.writer(outfile)
	writer1 = csv.writer(csv_file)
	data = {}
	content = {}
	act_content = {}
	count = 1
	csv.field_size_limit(sys.maxsize)

	for rows in islice(reader, 1, None):
		html = rows[1]
		act_content[count] = html
		soup = BeautifulSoup(html, "lxml")
		for script in soup(["script", "style"]):
		    script.extract()    # rip it out
		text = soup.get_text()
		lines = (line.strip() for line in text.splitlines())
		chunks = (phrase.strip() for line in lines for phrase in line.split("  ")) # break multi-headlines into a line each
		text = '\n'.join(chunk for chunk in chunks if chunk)    # drop blank lines
		text = text + rows[2]
		content[count] = text
		text = re.sub('[*./,+!:(;)?-]', '', text)
		text = re.sub("\d+", "", text)
		text = re.sub('[\s+]', '', text)
		data[count] = text
		count += 1

	objs = [(str(k), Simhash(get_features(v, width))) for k, v in data.items()]
	try:
		index = SimhashIndex(objs, k=thresh)
	except:
		print "Unexpected error: ", sys.exc_info()[0]
	cluster = {}
	lis = []
	for k,v in objs:
		if isPresent(k, lis):
			continue
		dups = index.get_near_dups(v)
		lis.extend(dups)
		cluster[k] = dups
		writer.writerow([k, dups])

	for key, value in content.items():
		writer1.writerow([key, value])

	with open('result.json', 'w') as fp:
		json.dump(act_content, fp)