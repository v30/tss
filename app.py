from flask import Flask, request, render_template, redirect, url_for, flash, g
from flasklogin import LoginManager
import logging
import sys
import csv
import random
import collections
import sqlite3
import jsbeautifier
from operator import itemgetter 

csv.field_size_limit(sys.maxsize)
reload(sys)
sys.setdefaultencoding("utf-8")

app = Flask (__name__)
app.secret_key = 'some_secret'
login_manager = LoginManager()
login_manager.init_app(app)

glob_dict = {}
glob_dom_dict = {}
glob_phone_dict = {}
opt = None
infile = None
reader = None
idx_file = None
session = True

DATABASE = 'index.db'

class User():

    def __init__(self):
        self.name = "admin"
        self.password = "cluster@123"

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return rv[0]
    #return (rv[0] if rv else None) if one else rv

def process(s):
	k = []
	c = 0
	s = s[1:-1]
	val = s.split(",")
	for i in val:
		if c==0:
			i = i[2:-1]
			c = 1
		else:
			i = i[3:-1]
		k.append(int(i))
	return k

@app.route('/', methods=['GET', 'POST'])
def login():
    global session
    session = False
    error = None
    if request.method == 'POST':
        if request.form['username'] != 'admin' or request.form['password'] != 'admin':
            error = 'Invalid Credentials. Please try again.'
        else:
            session = True
            return redirect(url_for('combo'))
    return render_template('login.html', error=error)


"""
@app.route("/", methods=["GET", "POST"])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']        
        if password == "cluster@123":
            user = User()
            login_user(user)
            return redirect(url_for('combo'))
        else:
            return abort(401)
    else:
        render_template('login.html', error=error)
"""

@app.route('/combo', methods=['GET', 'POST'])
def combo():
    if session == False:
        return redirect(url_for('login')) 
    error = None
    if request.method == 'POST':
        options = request.form['options']
        if options == None:
            error = 'Please select an option.'
        else:
            global opt
            opt = options
            file = 'cluster' + opt + '.csv'
            global idx_file
            idx_file = 'idx_html' + opt + '.csv'
            global infile
            infile = open(file, 'r')
            global reader
            reader = csv.reader(infile)
            return redirect(url_for('clusters'))
    return render_template('choice.html', error=error)

@app.route('/clusters')
def clusters():
    if session == False:
       return redirect(url_for('login')) 
    k = []
    dic = collections.OrderedDict()
    count = 1;
    infile.seek(0)
    for row in reader:
    	link = 'Cluster' + ' ' + str(count)
    	s = row[1]
    	k = process(s)
        doms = fetchdomains(k)
        phones = fetchPhones(k)
        glob_dom_dict[count] = doms
        glob_phone_dict[count] = phones
    	dic[link] = len(k)
    	glob_dict[count] = int(row[0])
    	count +=1
    return render_template('show_cluster_links.html', dic=dic)

def fetchPhones(k):
    phones = set()
    for i in k:
        d = query_db('select phone_numbers from scam_data where s_id = ?', [int(i)], one=True)
        s = ''.join(d)
        if s not in phones:
            phones.add(s)
    return phones    

def fetchdomains(k):
    domains = set()
    for i in k:
        d = query_db('select domain from scam_data where s_id = ?', [int(i)], one=True)
        s = ''.join(d)
        idx = s.rfind(".")
        s = s[0:idx]
        if s not in domains:
            domains.add(s)
    return domains

@app.route('/phone_numbers', methods=['GET'])
def phone_numbers():
    if session == False:
        return redirect(url_for('login'))
    key = request.args.get('link')
    key = key[7:]
    c = 1
    dic = collections.OrderedDict()
    lis = []
    for p in glob_phone_dict[int(key)]:
        if p not in lis:
            dic[c] = p
            c += 1
            lis.append(p)
    return render_template('phones.html', dic=dic)


@app.route('/similarity', methods=['GET'])
def similarity():
    if session == False:
        return redirect(url_for('login'))
    key = request.args.get('link')
    key = key[7:]
    jacs_d = collections.OrderedDict()
    jacs_p = collections.OrderedDict()
    A = glob_dom_dict[int(key)]
    for k,B in glob_dom_dict.items():
        j = float(len(A.intersection(B))) / len(A.union(B))
        if j > 0:
            s = 'Cluster' + ' ' + str(k)
            jacs_d[s] = j

    A = glob_phone_dict[int(key)]
    for k,B in glob_phone_dict.items():
        j = float(len(A.intersection(B))) / len(A.union(B))
        if j > 0:
            s = 'Cluster' + ' ' + str(k)
            jacs_p[s] = j
    return render_template('show_similarities.html' ,jacs_d=jacs_d ,jacs_p=jacs_p)


@app.route('/html_content', methods=['GET'])
def html_content():
    if session == False:
       return redirect(url_for('login')) 
    idx = request.args.get('id')
    html = query_db('select html_code from scam_data where s_id = ?', [int(idx)], one=True)
    f = open('templates/content.html', 'w')
    s = ''.join(html)
    f.write(s)
    f.close()
    return render_template('content.html')


@app.route('/html_code', methods=['GET'])
def html_code():
    if session == False:
       return redirect(url_for('login')) 
    idx = request.args.get('id')
    html = query_db('select html_code from scam_data where s_id = ?', [int(idx)], one=True)
    s = ''.join(html)
    s = '"{}"'.format(s)
    return render_template('code.html', s=s)
	
@app.route('/domains', methods=['GET'])
def domains():
    if session == False:
        return redirect(url_for('login'))
    key = request.args.get('link')
    key = key[7:]
    ids = []
    doms = collections.OrderedDict()
    domains = []
    infile.seek(0)
    for rows in reader:
        if int(rows[0]) == glob_dict[int(key)]:
            cl = rows[1]
            ids = process(cl)
            break
    c = 1
    for i in ids:
        d = query_db('select domain from scam_data where s_id = ?', [int(i)], one=True)
        s = ''.join(d)
        if s not in domains:
            domains.append(s)
    domains.sort()
    for d in domains:
        doms[c] = d
        c += 1
    return render_template('domains.html',doms=doms)


@app.route('/htext', methods=['GET'])
def html_text():
    if session == False:
       return redirect(url_for('login')) 
    key = request.args.get('link')
    key = key[7:]
    k = []
    c = 0
    num = 0;
    infile.seek(0)
    for rows in reader:
    	if int(rows[0]) == glob_dict[int(key)]:
    		s = rows[1]
    		k = process(s)
    		break

    length = len(k)
    ky = []
    cnt = 0
    if length > 3:
    	while (cnt < 4):
    		ky.append(random.choice(k))
    		cnt+=1
    else:
    	while length > 0:
    		ky.append(random.choice(k))
    		length-=1
    with open(idx_file) as csvfile:
    	r = csv.reader(csvfile)
    	lis = []
        d = collections.OrderedDict()
    	for row in r:
            if int(row[0]) in ky:
                d[int(row[0])] = row[1]
    			#lis.append(row[1])
        return render_template('htext.html', d=d)

if __name__ == "__main__":
    logging.basicConfig(filename='error.log',level=logging.DEBUG)
    app.run(host='0.0.0.0',threaded=True)
