from BeautifulSoup import BeautifulSoup
import csv
from itertools import islice
import re
from simhash import Simhash, SimhashIndex

def get_features(s):
    width = 4
    s = s.lower()
    s = re.sub(r'[^\w]+', '', s)
    lis = [s[i:i + width] for i in range(max(len(s) - width + 1, 1))]
    return [s[i:i + width] for i in range(max(len(s) - width + 1, 1))]


if __name__ == '__main__':
	
	infile = open("scam_unique.csv", 'r')
	outfile = open("html.txt", 'w')
	reader = csv.reader(infile)

	for rows in islice(reader,1,10):
		html_data = rows[2]
		cleantext = BeautifulSoup(html_data).text
		data = re.sub('[\s+]', '', cleantext)
		print Simhash(get_features(data)).value
