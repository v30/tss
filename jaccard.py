import csv
from itertools import islice
import sys
from nltk.corpus import words
import collections
import re

global_dict = collections.OrderedDict()

if __name__ == '__main__':
	
	infile = open('scam_mod.csv', 'r')
	reader = csv.reader(infile)
	outfile = open('words.csv', 'w')
	writer = csv.writer(outfile)
	csv.field_size_limit(sys.maxsize)
	#L = ["commandcomputer34.net", "help21techcommand.com", "1234techsupport.xyz"]
	word_list = words.words()
	c = 0
	lis = []
	for rows in islice(reader, 1, None):
		s = []
		dom = rows[0]
		dom = dom.split(".")
		dom = dom[0].split("-")
		dom = "".join(dom)
		dom = re.sub('\d','',dom)
		if dom in global_dict:
			continue
		sLen = len(dom)
		maxidx = 0
		i = 0
		while i < sLen:
			maxLen = 0
			maxW = ""
			i = maxidx
			for k in range(i+1, sLen+1):
				st = dom[i:k]
				if st in word_list:
					if maxLen < len(st):
						maxLen = len(st)
						maxidx = k
						maxW = st
			if maxW != '':
				s.append(maxW)
		global_dict[dom] = s
		print dom

	for k,v in global_dict.items():
		writer.writerow([k,v])

	#for i in range(len(lis)):
	#	k=0
	#	for j in range(i+1,len(lis)):
	#		k = float(len(lis[i].intersection(lis[j]))) / len(lis[i].union(lis[j]))
	#		print k